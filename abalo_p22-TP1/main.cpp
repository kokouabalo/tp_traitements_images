#include "control.h"
#include <stdio.h>
#include <string>
#include <iostream>
using namespace std;
//Affichage du message de bienvenue
void displayWelcome()
{
  cout<<"======================================================================================\n";
  cout<<"Bienvenue dans le programme Profil d'Intensité / Contraste V.1- TP1 : Kokou ABALO\n";
  cout<<"======================================================================================\n";
}

void displayMenu()
{
  cout<<"Veuillez taper:\n";
  cout<<"profil [option] image ---> pour afficher le profil\n";
  cout<<"contraste [option] image fonction ---> pour caculer le contraste\n";
  cout<<"\toption: 0 pour image à niveau de gris : 1 pour image en couleur\n";
  cout<<"\tfonction: 1, 2, 3, 4 ou 5\n";
  cout<<"\t\t1: Linaire\n";
  cout<<"\t\t2: Linaire avec saturation\n";
  cout<<"\t\t3: Linaire par morceaux\n";
  cout<<"\t\t4: Correction gama\n";
  cout<<"\t\t5: Egalisation de l'histogramme\n";
  cout<<"======================================================================================\n";
  cout<<"===> Passer une commande : ";
}

int main(int argc, char* argv[])
{
  Control control;
  int x1, y1, x2, y2;
  string cmd("");
  int option = 0;
  string image("image.jpg");
  int fonction = 1;
  string exit = "non";
  int hauteur, largeur, ligne;
  Mat imgRecup;
 
  displayWelcome();
  do
    {
      displayMenu();
      cin>>cmd >>option >>image;
      if(cmd.compare("profil") == 0)
	{
          cin>>ligne;
	  imgRecup = imread(image , 1);
	  hauteur = imgRecup.rows;
          largeur = imgRecup.cols;
           if (ligne > hauteur){
           do {
              cout<<"\n Entrer un numéro de ligne inférieur à ";
	      cout<<hauteur;
	      cin>>ligne;
              cout<<" : ";
              } while ( ligne > hauteur);}
           
	  x1=0, y1=ligne, x2=largeur, y2=ligne;
	  control.drawProfil(option, image, x1, y1, x2, y2);
	}
      else if(cmd.compare("contraste") == 0)
	{
	  cin>>fonction;
	  control.calcContrast(option, image, fonction);
	}
      else{
	cout<<"Aucune commande trouvée\n";
	continue;
      }
      cout<<"Trouvez les résultats dans le répertoire images_traitees/\n";
      cout<<"oui - pour quitter l'application: \n";
      cout<<"non - pour continuer d'utiliser l'application: \n";
      cin>>exit;
    } while (exit.compare("oui") != 0);
  return 0;
}
